{ generateKeyPairSync, publicEncrypt, privateDecrypt } = require 'crypto'
{ readFileSync, writeFileSync, statSync } = require 'fs'
path = require 'path'

module.exports = ( options ) ->

  keys = undefined

  @add 'role:crypto,action:encode', ( msg, respond ) ->
    { key = keys.publicKey, rawMessage } = msg
    rawMessage = JSON.stringify rawMessage if typeof rawMessage is 'object'

    encodedMessage = publicEncrypt( key, rawMessage ).toString 'base64'

    @act 'role:transaction,action:success', {
      ...msg
      encodedMessage
    }, respond

  @add 'role:crypto,action:decode', ( msg, respond ) ->
    { key = keys.privateKey, encodedMessage } = msg
    sourceEncodedMessage = Buffer.from encodedMessage, 'base64'

    decodedMessage = privateDecrypt( key, sourceEncodedMessage ).toString 'utf8'
    try
      decodedMessage = JSON.parse decodedMessage
    catch e

    @act 'role:transaction,action:success', {
      ...msg
      decodedMessage
    }, respond

  @add 'init:crypto', ( msg, respond ) ->
    # load keys
    keyspath = path.resolve process.cwd(), '.keys'

    try
      stats = statSync keyspath
      keysSource = readFileSync keyspath
      keys = JSON.parse keysSource
      respond null
    catch e
      keys = { publicKey, privateKey } = generateKeyPairSync 'rsa',
        modulusLength: 4096
        publicKeyEncoding:
          type: 'spki'
          format: 'pem'
        privateKeyEncoding:
          type: 'pkcs8'
          format: 'pem'
      writeFileSync keyspath, JSON.stringify keys
      respond null
